Configuring the database
========================

dbconfig-common should've asked you for database credentials on install. If
you've opted out of using dbconfig-common, then you'll need to set DATABASE_URL
manually in /etc/miniflux/miniflux.conf.

See https://miniflux.app/docs/database.html

Creating the admin account
==========================

When you first install Miniflux, there will be no accounts yet. To create the
first admin account, you can use `sudo miniflux -c /etc/miniflux/miniflux.conf
-create-admin` (this requires the database to be configured).

Alternatively, you can set CREATE_ADMIN, ADMIN_USERNAME and ADMIN_PASSWORD in
/etc/miniflux/miniflux.conf. (See https://miniflux.app/docs/configuration.html)
